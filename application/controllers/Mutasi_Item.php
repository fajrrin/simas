<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutasi_Item extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'mutasi_item');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Mutasi_Item";
		$data['table'] = $this->model->get(array('id','item_id','tipe','keterangan','tanggal','status'));
		$data['field'] = array('Nama Item','Tipe','Keterangan','Tanggal','Status','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'mutasi_item/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'mutasi_item/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['item_id'] = $post['item_id'];
			$object['tipe'] = $post['tipe'];
			$object['keterangan'] = $post['keterangan'];
			$object['tanggal'] = $post['tanggal'];
			$object['status'] = $post['status'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['item_id'] = $post['item_id'];
			$object['tipe'] = $post['tipe'];
			$object['keterangan'] = $post['keterangan'];
			$object['tanggal'] = $post['tanggal'];
			$object['status'] = $post['status'];
			$this->model->insert_data($object);
		}
		redirect('mutasi_item');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('mutasi_item');
	}

}

/* End of file Mutasi_Item.php */
/* Location: ./application/controllers/Mutasi_Item.php */