<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'pembayaran');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Pembayaran";
		$data['table'] = $this->model->get(array('id','harga_id','status','tipe_pasien'));
		$data['field'] = array('Harga','Status','Tipe Pasien','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'pembayaran/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'pembayaran/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['harga_id'] = $post['harga_id'];
			$object['status'] = $post['status'];
			$object['tipe_pasien'] = $post['tipe_pasien'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['harga_id'] = $post['harga_id'];
			$object['status'] = $post['status'];
			$object['tipe_pasien'] = $post['tipe_pasien'];
			$this->model->insert_data($object);
		}
		redirect('pembayaran');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('pembayaran');
	}

}

/* End of file Pembayaran.php */
/* Location: ./application/controllers/Pembayaran.php */