<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ruangan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'ruangan');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Ruangan";
		$data['table'] = $this->model->get(array('id','nama','deskripsi'));
		$data['field'] = array('Nama Ruangan','Deskripsi','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'ruangan/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'ruangan/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['nama'] = $post['nama'];
			$object['deskripsi'] = $post['deskripsi'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['nama'] = $post['nama'];
			$object['deskripsi'] = $post['deskripsi'];
			$this->model->insert_data($object);
		}
		redirect('ruangan');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('ruangan');
	}

}

/* End of file Ruangan.php */
/* Location: ./application/controllers/Ruangan.php */