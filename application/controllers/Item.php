<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'item');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Item";
		$data['table'] = $this->model->get(array('id','nama','kuantitas','harga_satuan','jenis_item_id','satuan'));
		$data['field'] = array('Nama','Kuantitas','Harga Satuan','Jenis Item','Satuan','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'item/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'item/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['nama'] = $post['nama'];
			$object['kuantitas'] = $post['kuantitas'];
			$object['harga_satuan'] = $post['harga_satuan'];
			$object['jenis_item_id'] = $post['jenis_item_id'];
			$object['satuan'] = $post['satuan'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['nama'] = $post['nama'];
			$object['kuantitas'] = $post['kuantitas'];
			$object['harga_satuan'] = $post['harga_satuan'];
			$object['jenis_item_id'] = $post['jenis_item_id'];
			$object['satuan'] = $post['satuan'];
			$this->model->insert_data($object);
		}
		redirect('item');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('item');
	}

}

/* End of file Item.php */
/* Location: ./application/controllers/Item.php */