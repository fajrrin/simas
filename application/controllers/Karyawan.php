<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'karyawan');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Karyawan";
		$data['table'] = $this->model->get(array('id','nama','tanggal_lahir','alamat','jabatan_id'));
		$data['field'] = array('Nama Karyawan','Tanggal Lahir','Alamat','Jabatan','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'karyawan/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'karyawan/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['nama'] = $post['nama'];
			$object['tempat_lahir'] = $post['tempat_lahir'];
			$object['tanggal_lahir'] = $post['tanggal_lahir'];
			$object['jenis_kelamin'] = $post['jenis_kelamin'];
			$object['gol_darah'] = $post['gol_darah'];
			$object['agama'] = $post['agama'];
			$object['alamat'] = $post['alamat'];
			$object['jabatan_id'] = $post['jabatan_id'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['nama'] = $post['nama'];
			$object['tempat_lahir'] = $post['tempat_lahir'];
			$object['tanggal_lahir'] = $post['tanggal_lahir'];
			$object['jenis_kelamin'] = $post['jenis_kelamin'];
			$object['gol_darah'] = $post['gol_darah'];
			$object['agama'] = $post['agama'];
			$object['alamat'] = $post['alamat'];
			$object['jabatan_id'] = $post['jabatan_id'];
			$this->model->insert_data($object);
		}
		redirect('karyawan');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('karyawan');
	}

}

/* End of file Karyawan.php */
/* Location: ./application/controllers/Karyawan.php */