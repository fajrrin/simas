<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_tunggu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'daftar_tunggu');
	}

	public function test($id=1,$days=1)
	{
            $kode = date('Ymd');
            $db = $this->db->order_by('id','desc')->get('daftar_tunggu')->result_array();
            print_r($db);
            $i = substr((!empty($db[0]['nomor_urut']) ? $db[0]['nomor_urut'] : 0),9,6)+1; //meneruskan no urut 
            //$i  = count($db)+1; 
            $kode .= substr("000000",0,(6 - strlen($i))).$i; 
            echo $kode;

	}


	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));

		$data = array();
		$data['controller'] = "Daftar_Tunggu";
		$data['table'] = $this->model->get(array('id','tanggal','pasien_id','nomor_urut','keluhan','ruangan_id'));
		$data['field'] = array('Tanggal','Nama Pasien','Nomor Urut','Keluhan','Nama Ruangan','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'daftar_tunggu/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'daftar_tunggu/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}

		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['tanggal'] = $post['tanggal'];
			$object['pasien_id'] = $post['pasien_id'];
			$object['nomor_urut'] = $post['nomor_urut'];
			$object['keluhan'] = $post['keluhan'];
			$object['ruangan_id'] = $post['ruangan_id'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['tanggal'] = $post['tanggal'];
			$object['pasien_id'] = $post['pasien_id'];
			$object['nomor_urut'] = $post['nomor_urut'];
			$object['keluhan'] = $post['keluhan'];
			$object['ruangan_id'] = $post['ruangan_id'];
			$this->model->insert_data($object);
		}
		redirect('daftar_tunggu');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('daftar_tunggu');
	}

}

/* End of file Daftar_Tunggu.php */
/* Location: ./application/controllers/Daftar_Tunggu.php */