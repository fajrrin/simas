<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'pelayanan');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Pelayanan";
		$data['table'] = $this->model->get(array('id','nama','harga'));
		$data['field'] = array('Nama Pelayanan','Harga','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'pelayanan/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'pelayanan/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['nama'] = $post['nama'];
			$object['harga'] = $post['harga'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['nama'] = $post['nama'];
			$object['harga'] = $post['harga'];
			$this->model->insert_data($object);
		}
		redirect('pelayanan');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('pelayanan');
	}

}

/* End of file Pelayanan.php */
/* Location: ./application/controllers/Pelayanan.php */