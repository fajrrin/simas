<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'blog');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Blog";
		$data['table'] = $this->model->get(array('id','title','content','tanggal','karyawan_id'));
		$data['field'] = array('Judul','Konten','Tanggal','Pengarang','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'blog/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'blog/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}		
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['title'] = $post['title'];
			$object['content'] = $post['content'];
			$object['tanggal'] = $post['tanggal'];
			$object['karyawan_id'] = $post['karyawan_id'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['title'] = $post['title'];
			$object['content'] = $post['content'];
			$object['tanggal'] = $post['tanggal'];
			$object['karyawan_id'] = $post['karyawan_id'];
			$this->model->insert_data($object);
		}
		redirect('blog');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('blog');
	}

}

/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */