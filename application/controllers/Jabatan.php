<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'jabatan');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Jabatan";
		$data['table'] = $this->model->get(array('id','nama'));
		$data['field'] = array('Nama Jabatan','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'jabatan/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'jabatan/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}	
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['nama'] = $post['nama'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['nama'] = $post['nama'];
			$this->model->insert_data($object);
		}
		redirect('jabatan');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('jabatan');
	}

}

/* End of file Jabatan.php */
/* Location: ./application/controllers/Jabatan.php */