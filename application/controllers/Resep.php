<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resep extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'resep');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Resep";
		$data['table'] = $this->model->get(array('id','resep_detail_id','total_harga_resep'));
		$data['field'] = array('Detail Resep','Total Harga','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'resep/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'resep/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['resep_detail_id'] = $post['resep_detail_id'];
			$object['total_harga_resep'] = $post['total_harga_resep'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['resep_detail_id'] = $post['resep_detail_id'];
			$object['total_harga_resep'] = $post['total_harga_resep'];
			$this->model->insert_data($object);
		}
		redirect('resep');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('resep');
	}

}

/* End of file Resep.php */
/* Location: ./application/controllers/Resep.php */