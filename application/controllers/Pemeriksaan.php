<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'pemeriksaan');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Pemeriksaan";
		$data['table'] = $this->model->get(array('id','daftar_tunggu_id','karyawan_id','pasien_id','tanggal'));
		$data['field'] = array('Daftar Tunggu','Nama Karyawan','Nama Pasien','Tanggal','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'pemeriksaan/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'pemeriksaan/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['daftar_tunggu_id'] = $post['daftar_tunggu_id'];
			$object['ruangan_id'] = $post['ruangan_id'];
			$object['karyawan_id'] = $post['karyawan_id'];
			$object['pasien_id'] = $post['pasien_id'];
			$object['tanggal'] = $post['tanggal'];
			$object['resep_id'] = $post['resep_id'];
			$object['diagnosa'] = $post['diagnosa'];
			$object['catatan'] = $post['catatan'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['daftar_tunggu_id'] = $post['daftar_tunggu_id'];
			$object['ruangan_id'] = $post['ruangan_id'];
			$object['karyawan_id'] = $post['karyawan_id'];
			$object['pasien_id'] = $post['pasien_id'];
			$object['tanggal'] = $post['tanggal'];
			$object['resep_id'] = $post['resep_id'];
			$object['diagnosa'] = $post['diagnosa'];
			$object['catatan'] = $post['catatan'];
			$this->model->insert_data($object);
		}
		redirect('pemeriksaan');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('pemeriksaan');
	}

}

/* End of file Pemeriksaan.php */
/* Location: ./application/controllers/Pemeriksaan.php */