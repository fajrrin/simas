<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komentar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'komentar');
	}

	public function test()
	{
		echo controller();
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Komentar";
		$data['table'] = $this->model->get(array('id','nama','blog_id','content'));
		$data['field'] = array('Nama Komentar',,'Nama Blog','Konten','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'komentar/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = 'komentar/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}
		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['nama'] = $post['nama'];
			$object['blog_id'] = $post['blog_id'];
			$object['content'] = $post['content'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			echo $post['id'];
			$object['nama'] = $post['nama'];
			$object['blog_id'] = $post['blog_id'];
			$object['content'] = $post['content'];
			$this->model->insert_data($object);
		}
		redirect('komentar');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('komentar');
	}

}

/* End of file Komentar.php */
/* Location: ./application/controllers/Komentar.php */