<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model','',FALSE,'pasien');
	}

	public function index()
	{
		//print_r($this->model->get(array('nama','nomor_kartu')));
		
		$data = array();
		$data['controller'] = "Pasien";
		$data['table'] = $this->model->get(array('id','nomor_kartu','nama','no_bpjs','alamat'));
		$data['field'] = array('No Kartu','Nama','No BPJS','Alamat','Action');
		$data['content'] = 'template/table';
		$data['data'] = 'pasien/index';
		$this->load->view('template/main',$data,FALSE);
	}

	public function add($id="")
	{
		$data = array();
		$data['content'] = controller().'/add';
		$query = $this->model->get_where(array('id'=>$id));
		if ($query) {
			$data['query'] 	= $query[0];
		}
		else {
			$data['query'] = "";
		}

		$this->load->view('template/main',$data,FALSE);
	}

	public function save()
	{
		$object = array();
		$post = $this->input->post();

		if (!empty($post['id'])) {
			$object['nomor_kartu'] = $post['no_kartu'];
			$object['nama'] = $post['nama'];
			$object['no_bpjs'] = $post['no_bpjs'];
			$object['alamat'] = $post['alamat'];
			$this->model->update_data($object,array("id"=>$post['id']));
		}
		else
		{
			$object['nomor_kartu'] = $post['no_kartu'];
			$object['nama'] = $post['nama'];
			$object['no_bpjs'] = $post['no_bpjs'];
			$object['alamat'] = $post['alamat'];
			$this->model->insert_data($object);
		}
		redirect('pasien');
	}

	public function delete($id="")
	{
		$this->model->delete_data(array('id'=>$id));
		redirect('pasien');
	}

}

/* End of file Pasien.php */
/* Location: ./application/controllers/Pasien.php */