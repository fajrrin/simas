
 <div class="position-center">
  <form class="form-horizontal" role="form" action="<?php echo base_url("index.php/".controller()."/save")?>" method="post">

    <?php echo input_hidden('id',!empty($query) ? $query['id'] : "") ?>

    <div class="form-group">
      <?php echo label('Nama Item') ?>
      <div class="col-lg-10">
        <?php echo input_text('nama',!empty($query) ? $query['nama'] : "","Nama Item") ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Kuantitas') ?>
      <div class="col-lg-10">
        <?php echo input_text('kuantitas',!empty($query) ? $query['kuantitas'] : "","Kuantitas") ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Harga Satuan') ?>
      <div class="col-lg-10">
        <?php echo input_text('harga_satuan',!empty($query) ? $query['harga_satuan'] : "",'Harga Satuan') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Jenis Item') ?>
      <div class="col-lg-10">
        <?php echo input_text('jenis_item_id',!empty($query) ? $query['jenis_item_id'] : "",'Jenis Item') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Satuan') ?>
      <div class="col-lg-10">
        <?php echo input_text('satuan',!empty($query) ? $query['satuan'] : "",'Satuan') ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
        <button type="submit" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Data</button>
      </div>
    </div>

  </form>
</div>