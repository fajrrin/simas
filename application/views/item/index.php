<?php 
foreach ($table as $row) {
	?>
	<tr class="">
		<td><?php echo $row['nama'] ?></td>
		<td><?php echo $row['kuantitas'] ?></td>
		<td><?php echo $row['harga_satuan'] ?></td>
		<td><?php echo $row['jenis_item_id'] ?></td>
		<td><?php echo $row['satuan'] ?></td>
		<td class="text-center">
		<a class="btn btn-primary btn-md" href="<?php echo base_url("index.php/".controller()."/add/".$row['id']." ")?>"><span class="fa fa-pencil"></span></a>
			<a class="btn btn-danger btn-md" href="<?php echo base_url("index.php/".controller()."/delete/".$row['id']." ")?>"><span class="fa fa-trash-o"></span></a>
		</td>
	</tr>
	<?php
}
?>
