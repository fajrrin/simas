
 <div class="position-center">
  <form class="form-horizontal" role="form" action="<?php echo base_url("index.php/".controller()."/save")?>" method="post">

    <?php echo input_hidden('id',!empty($query) ? $query['id'] : "") ?>

    <div class="form-group">
      <?php echo label('Jabatan') ?>
      <div class="col-lg-10">
        <?php echo input_text('nama',!empty($query) ? $query['nama'] : "",'Jabatan') ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
        <button type="submit" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Data</button>
      </div>
    </div>

  </form>
</div>