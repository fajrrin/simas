 <div class="position-center">
  <form class="form-horizontal" role="form" action="<?php echo base_url("index.php/".controller()."/save")?>" method="post">

    <?php echo input_hidden('id',!empty($query) ? $query['id'] : "") ?>

    <div class="form-group">
      <?php echo label('Title') ?>
      <div class="col-lg-10">
        <?php echo input_text('title',!empty($query) ? $query['title'] : "",'Judul') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Konten') ?>
      <div class="col-lg-10">
        <?php echo input_textarea('content',!empty($query) ? $query['content'] : "",'Nomor Kartu') ?>
      </div>
    </div>
    
    <div class="form-group">
      <?php echo label('Tanggal') ?>
      <div class="col-lg-10">
        <?php echo input_date('tanggal',!empty($query) ? $query['tanggal'] : "") ?>
      </div>
    </div>
    
    <div class="form-group">
      <?php echo label('Pengarang') ?>
      <div class="col-lg-10">
        <?php echo input_text('karyawan_id',!empty($query) ? $query['karyawan_id'] : "",'Nama Pengarang') ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
        <button type="submit" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Data</button>
      </div>
    </div>

  </form>
</div>