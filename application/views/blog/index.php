<?php 
foreach ($table as $row) {
	?>
	<tr class="">
		<td><?php echo $row['title'] ?></td>
		<td><?php echo $row['content'] ?></td>
		<td><?php echo $row['tanggal'] ?></td>
		<td><?php echo $row['karyawan_id'] ?></td>
		<td class="text-center">
			<a class="btn btn-primary btn-md" href="<?php echo base_url("index.php/".controller()."/add/".$row['id']." ")?>"><span class="fa fa-pencil"></span></a>
			<a class="btn btn-danger btn-md" href="<?php echo base_url("index.php/".controller()."/delete/".$row['id']." ")?>"><span class="fa fa-trash-o"></span></a>
		</td>
	</tr>
	<?php
}
?>
