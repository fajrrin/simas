
 <div class="position-center">
  <form class="form-horizontal" role="form" action="<?php echo base_url("index.php/".controller()."/save")?>" method="post">

    <?php echo input_hidden('id',!empty($query) ? $query['id'] : "") ?>

    <div class="form-group">
      <?php echo label('Tanggal') ?>
      <div class="col-lg-10">
        <?php echo input_date('tanggal',!empty($query) ? $query['tanggal'] : date('Y-m-d')) ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Nama Pasien') ?>
      <div class="col-lg-10">
        <?php echo input_text('pasien_id',!empty($query) ? $query['pasien_id'] : "",'Pasien') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Nomor Urut') ?>
      <div class="col-lg-10">
        <?php echo input_text('nomor_urut',!empty($query) ? $query['nomor_urut'] : nomor_urut(),'Nomor Urut') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Keluhan') ?>
      <div class="col-lg-10">
        <?php echo input_textarea('keluhan',!empty($query) ? $query['keluhan'] : "",'Keluhan') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Ruangan') ?>
      <div class="col-lg-10">
        <?php echo input_text('ruangan_id',!empty($query) ? $query['ruangan_id'] : "",'Ruangan') ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
        <button type="submit" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Data</button>
      </div>
    </div>

  </form>
</div>