 <div class="position-center">
  <form class="form-horizontal" role="form" action="<?php echo base_url("index.php/".controller()."/save")?>" method="post">

    <?php echo input_hidden('id',!empty($query) ? $query['id'] : "") ?>

    <!-- email -->
    <div class="form-group">
      <?php echo label('Nomor Kartu') ?>
      <div class="col-lg-10">
        <?php echo input_text('no_kartu',!empty($query) ? $query['nomor_kartu'] : "",'Nomor Kartu') ?>
      </div>
    </div>

    <!-- password -->
    <div class="form-group">
      <?php echo label('Nama') ?>
      <div class="col-lg-10">
        <?php echo input_text('nama',!empty($query) ? $query['nama'] : "",'Nama') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Nomor BPJS') ?>
      <div class="col-lg-10">
        <?php echo input_text('no_bpjs',!empty($query) ? $query['no_bpjs'] : "",'Nomor BPJS') ?>
      </div>
    </div>

    <div class="form-group">
      <?php echo label('Alamat') ?>
      <div class="col-lg-10">
        <?php echo input_textarea('alamat',!empty($query) ? $query['alamat'] : "",'Alamat') ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
        <button type="submit" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Data</button>
      </div>
    </div>

  </form>
</div>