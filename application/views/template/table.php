 <div class="adv-table editable-table ">
          <div class="clearfix">
            <div class="btn-group">
              <a href="<?php echo base_url("index.php/".controller()."/add") ?>"><button class="btn btn-primary">
                Add New <i class="fa fa-plus"></i>
              </button>
            </a>
          </div>
          <div class="btn-group pull-right">
            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu pull-right">
              <li><a href="#">Print</a></li>
              <li><a href="#">Save as PDF</a></li>
              <li><a href="#">Export to Excel</a></li>
            </ul>
          </div>
        </div>
        <div class="space15"></div>
        <table class="table table-striped table-hover table-bordered" id="editable-sample">
          <thead>
            <tr>
              <?php
              foreach ($field as $field) {
                echo "<th class='text-center'>".$field."</th>";
              }
              ?>
            </tr>
          </thead>
          <tbody>    
              <?php echo $this->load->view($data,'',TRUE); ?>
          </tbody>
        </table>
      </div>