<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="ima;ges/favicon.png">

  <title>SIMAS</title>

  <!--Core CSS -->
  <?php
  echo load_css('bs3/css/bootstrap.min.css','stylesheet');
  echo load_css('css/bootstrap-reset.css','stylesheet');
  echo load_css('font-awesome/css/font-awesome.css','stylesheet');

  //Custom styles for this template
  echo load_css('css/style.css','stylesheet');
  echo load_css('css/style-responsive.css','stylesheet');


  ?>

  <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="full-width">

    <section id="container" class="hr-menu">
      <!--header start-->
      <header class="header fixed-top">

        <?php echo $this->load->view('template/navigation', '', TRUE); ?>
        
      </header>
      <!--header end-->
      <!--sidebar start-->

      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <!-- page start-->
          <div class="row">
            <div class="col-lg-12">
              <!--breadcrumbs start -->
              <ul class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
              </ul>
              <!--breadcrumbs end -->
            </div>
          </div>
         <?php echo $this->load->view('template/content', '', TRUE); ?>
          
            </section>
          </div>
        </div>

        </section>
      </div>
    </div>
    <!-- page end-->
  </section>
</section>
<!--main content end-->
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<!--footer start-->
<footer class="footer-section">
  <div class="text-center">
    Sistem Informasi Puskesmas © 2015 All Right Reserved.
    <a href="#" class="go-top">
      <i class="fa fa-angle-up"></i>
    </a>
  </div>
  <?php echo $this->benchmark->elapsed_time();?>

</footer>
<!--footer end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->

<!--Core js-->
<?php 
echo load_js('js/jquery.js');
echo load_js('bs3/js/bootstrap.min.js');
echo load_js('js/hover-dropdown.js');
echo load_js('js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js');
echo load_js('js/jquery.nicescroll.js');
//common script init for all pages
echo load_js('js/scripts.js');
?>


<style type="text/css">
  @font-face {
    font-family: 'Continuum Light';
    font-style: normal;
    src: url("<?php echo base_url('assets/font/contl.ttf') ?>") format('truetype');
  }
</style>  

</body>
</html>
