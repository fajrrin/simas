
<div class="row">
  <div class="col-sm-12">
    <section class="panel">
      <header class="panel-heading">
        <h2>Data Pasien</h2>
        <span class="tools pull-right">
          <a href="javascript:;" class="fa fa-chevron-down"></a>
          <a href="javascript:;" class="fa fa-cog"></a>
          <a href="javascript:;" class="fa fa-times"></a>
        </span>
      </header>
      <div class="panel-body">
      <?php echo  $this->load->view($content,'',TRUE); ?>
      </div>