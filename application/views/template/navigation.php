  <div class="navbar-header">
    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="fa fa-bars"></span>
    </button>

    <!--logo start-->
    <!--logo start-->
    <div class="brand ">
      <a href="index.html" class="logo">
        <p style="font-family:Continuum Light; font-size:60px; color:#fff; text-transform:none; margin-top:-25px; margin-left:15px;">simas</p>                  
      </a>
    </div>
    <!--logo end-->
    <!--logo end-->
    <div class="horizontal-menu navbar-collapse collapse ">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.html">Dashboard</a></li>
        <li class="dropdown">
          <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#">Master <b class=" fa fa-angle-down"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo base_url()?>index.php/pasien">Pasien</a></li>
            <li><a href="<?php echo base_url()?>index.php/karyawan">Pegawai</a></li>
            <li><a href="<?php echo base_url()?>index.php/pelayanan">Pelayanan</a></li>
            <li><a href="<?php echo base_url()?>index.php/item">Item</a></li>
            <li><a href="<?php echo base_url()?>index.php/ruangan">Ruangan</a></li>
          </ul>
        </li>
        <li><a href="<?php echo base_url()?>index.php/daftar_tunggu">Daftar Tunggu</a></li>
        <li><a href="<?php echo base_url()?>index.php/pemeriksaan">Pemeriksaan</a></li>
        <li><a href="<?php echo base_url()?>index.php/blog">Blog</a></li>
      </ul>
    </div>

    <div class="top-nav hr-top-nav">
      <ul class="nav pull-right top-menu">
        <!-- user login dropdown start-->              
        <li class="dropdown">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <img alt="" src="images/aziz.jpg">
            <span class="username">Aziz Melgiansyah</span>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu extended logout">
            <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
            <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
            <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>
            <li><a href="login.html"><i class="fa fa-key"></i> Log Out</a></li>
          </ul>
        </li>
        <!-- user login dropdown end -->
      </ul>
    </div>

  </div>
