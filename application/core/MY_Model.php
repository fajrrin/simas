<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	var $table = "";
	var $primary_key = "";

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function get($column="",$sort="",$order="")
	{
		$this->db->select($column);
		$this->db->order_by($sort,$order);
		$query = $this->db->get($this->table);
		return $query->result_array();
	}

	public function get_where($params="",$sort="",$order="")
	{
		$this->db->order_by($sort,$order);
		$this->db->where($params);
		$query = $this->db->get($this->table);
		return $query->result_array();
	}

	public function get_select($sort="",$order="",$fields="")
	{
		$this->db->order_by($sort,$order);
		$this->db->select(array($fields));
		$query = $this->db->get($this->table);
	}

	public function insert_data($data="")
	{
		$this->db->insert($this->table, $data);
	}

	public function update_data($data="",$params="")
	{
		$this->db->update($this->table, $data, $params);
	}

	public function delete_data($params="")
	{
		$this->db->delete($this->table, $params);
	}

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */