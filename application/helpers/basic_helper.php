<?php

if ( ! function_exists('load_css'))
{
	function load_css($file="",$rel="")
	{
		$query =  "<link href=".base_url('assets/'.$file)." rel=".$rel.">";
		return $query;
	}
}

if ( ! function_exists('load_js'))
{
	function load_js($file="")
	{
		$query = "<script src=".base_url('assets/'.$file)."></script>";
		return $query;
	}
}

if ( ! function_exists('controller'))
{
	function controller()
	{
		$CI =& get_instance();
		$query = $CI->uri->segment(1);
		return $query;
	}
}

if ( ! function_exists('label'))
{
	function label($name="")
	{
		$query = "<label class='col-lg-2 col-sm-2 control-label' style='color:#000000;'>".$name."</label>";
		return $query;
	}
}

//Input Helper
if ( ! function_exists('input_text'))
{
	function input_text($name="",$value="",$placeholder="")
	{
		$query = "<input type='text' name='".$name."' value='".$value."' class='form-control' id='".$name."' placeholder='".$placeholder."' style='color:#000000;'>";
		return $query;
	}
}

if ( ! function_exists('input_hidden'))
{
	function input_hidden($name="",$value="")
	{
		$query = "<input type='hidden' name='".$name."' value='".$value."' class='form-control' id='".$name."'>";
		return $query;
	}
}

if ( ! function_exists('input_date'))
{
	function input_date($name="",$value="")
	{
		$query = "<input type='date' name='".$name."' value='".$value."' class='form-control' id='".$name."' style='color:#000000;'>";
		return $query;
	}
}

if ( ! function_exists('input_textarea'))
{
	function input_textarea($name="",$value="",$placeholder="")
	{
		$query = "<textarea name='".$name."' class='form-control' rows='6' style='color:#000000;' placeholder='Konten'>".$value."</textarea>";
		return $query;
	}
}

if ( ! function_exists('nomor_urut'))
{
	function nomor_urut()
	{
		$CI =& get_instance();
		$CI->load->model('model');
		$kode = date('Ymd');
        $db = $CI->db->order_by('id','desc')->get('daftar_tunggu')->result_array();
        $i = substr((!empty($db[0]['nomor_urut']) ? $db[0]['nomor_urut'] : 0),9,6)+1; //meneruskan no urut 
        //$i  = count($db)+1; 
        $kode .= substr("-00",0,(4 - strlen($i))).$i; 
        return $kode;
    }
}

