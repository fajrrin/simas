<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends MY_Model { 

	function __construct($table="")
	{
		parent::__construct();
		$this->table = $table;
	}

}

/* End of file Pasien.php */
/* Location: ./application/models/Pasien.php */